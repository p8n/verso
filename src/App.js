import React from 'react';
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { NonIdealState } from '@blueprintjs/core';

import Sidebar from './Sidebar';
import Loader from './Loader';
import Nav from './Nav';
import Listing from './Listing';

import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      backend: props.backend,
    };
    this.fetchBackendState = this.fetchBackendState.bind(this);
  }

  componentDidMount() {
    setTimeout(
      () => this.fetchBackendState(),
      200
    );
  }

  fetchBackendState() {
    if (this.state.backend && this.state.backend.applyUpdates()) {
      this.setState({
        backend: this.state.backend,
      });
    }

    setTimeout(
      () => this.fetchBackendState(),
      200
    );
  }

  render() {
    const backend = this.state.backend;

    return (
      <Loader
        key="app-loader"
        async={backend.functions}
      >
        {(functs) => {
          return (
            <Router>
              <div className="App">
                <Switch>
                  <Route exact path="/" render={props =>
                    [
                      <Nav
                        key="app-navbar"
                        selectedFunc={null}
                        backend={backend}
                      />,
                      <Sidebar
                        key="app-sidebar"
                        functions={functs}
                        selectedFunc={null}
                        className="App-panel"
                      />,
                      <NonIdealState
                        icon="remove"
                        title="No function selected"
                        description="Pick one from the sidebar on the left."
                      />
                    ]
                  } />

                  <Route
                    exact path="/:func/:row?"
                    render={props => {
                      const uuid = props.match.params["func"];
                      const func = functs.value[uuid];

                      if (func) {
                        return [
                          <Nav
                            key="app-navbar"
                            selectedFunc={func}
                            backend={backend}
                          />,
                          <Sidebar
                            key="app-sidebar"
                            functions={functs}
                            selectedFunc={uuid}
                            className="App-panel"
                          />,
                          <Loader
                            key="app-listing-loader"
                            fetch={() => backend.fetchListing(uuid)}
                            async={func.listing}
                          >
                            {(listing) => {
                              let row = props.match.params["row"];

                              if (row === "" || row === undefined) {
                                row = listing.value.entryPoint;
                              } else {
                                // XXX: more schemes
                                row = parseInt(row, 10);
                              }

                              return (
                                <Listing
                                  scrollToRow={row}
                                  func={func}
                                  listing={listing}
                                  backend={backend}
                                  className="App-panel"
                                  fullLineHeight={25}
                                  halfLineHeight={13}
                                  addressColumnWidth={70 + 75}
                                  mnemonicPaddingLeft={12}
                                  mnemonicPaddingRight={10}
                                />
                              );
                            }}
                          </Loader>
                        ];
                      } else {
                        return [
                          <Nav
                            key="app-navbar"
                            selectedFunc={null}
                          />,
                          <Sidebar
                            key="app-sidebar"
                            functions={functs}
                            selectedFunc={uuid}
                            className="App-panel"
                          />,
                          <div key="app-errormsg">
                            Unknown function {props.location.pathname}
                          </div>
                        ];
                      }
                    }}
                  />

                  <Route render={props => <div>Unknown URL</div>} />
                </Switch>
              </div>
            </Router>
          );
        }}
      </Loader>
    );
  }
}

export default App;
