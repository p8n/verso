import React from 'react';
import { Spinner } from '@blueprintjs/core';

class Loader extends React.Component {
  render() {
    const { async, className, children } = this.props;

    if (async.state === "present") {
      return children({ value: async.value, variable: async.version });
    } else {
      return (
        <div className={[className,"Listing","Listing-placeholder"].join(" ")}>
          <Spinner size={95} />
        </div>
      );
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { async, fetch } = this.props;

    if (async && async.state === "missing") {
      fetch();
    }
  }
}
export default Loader;
