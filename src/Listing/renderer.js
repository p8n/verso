import React from 'react';
import ReactHoverObserver from 'react-hover-observer';
import { CellMeasurer } from 'react-virtualized';

import Arrows from './arrows.js';

function getCellProps(child) {
  if (child.type === CellMeasurer) {
    return child.props.children.props;
  } else {
    return child.props;
  }
}

function arrowRenderer(children, {rowStartIndex, rowStopIndex }, activeArrows, halfLineHeight, fullLineHeight) {
  const first = getCellProps(children[0]).style;
  const last = getCellProps(children[children.length - 1]).style;

  if (!first || !last) {
    return children;
  }

  const height = last.top + last.height - first.top;

  if (Number.isNaN(height)) {
    return children;
  }

  const style = {
    position: "absolute",
    top: first.top.toString() + "px",
    height: (last.top + last.height - first.top).toString() + "px",
    width: "145px",
  };

  let myActiveArrows = {};
  let freeTracks = [...Array(8).keys()];

  // new activeArrows
  for (let i = 0; i < children.length; i += 1) {
    const me = i + rowStartIndex;
    const myProps = getCellProps(children[i]);

    if (myProps.to !== "") {
      const other = parseInt(myProps.from ? myProps.from : myProps.to, 10);

      if (!Number.isNaN(other)) {
        const j = other - rowStartIndex;
        let otherEndpoint = {
          type: 'incomplete',
          direction: j < i ? 'up' : 'down',
        };

        if (j >= 0 && j < children.length) {
          const otherProps = getCellProps(children[j]);

          if (otherProps) {
            otherEndpoint = {
              type: 'complete',
              y: otherProps.style.top - first.top,
            };
          }
        }

        // skip incomplete
        if (otherEndpoint.type === 'incomplete') {
          continue;
        }

        const arrowId = i > j
          ? (other.toString() + "--" + me.toString())
          : (me.toString() + "--" + other.toString());

        if (myActiveArrows[arrowId] !== undefined) {
          continue;
        }

        const thisEndpoint = {
          type: 'complete',
          y: myProps.style.top - first.top,
        };
        const branch = myProps.from ? otherEndpoint : thisEndpoint;
        const location = myProps.to ? otherEndpoint : thisEndpoint;

        if (activeArrows[arrowId]) {
          const track = activeArrows[arrowId].track
          const trackIdx = freeTracks.indexOf(track);

          if (trackIdx !== -1) {
            freeTracks.splice(trackIdx, 1);

            myActiveArrows[arrowId] = {
              branch: branch,
              location: location,
              track: track,
            };

            continue;
          }
        }

        myActiveArrows[arrowId] = {
          branch: branch,
          location: location,
        };
      }
    }
  }

  activeArrows = myActiveArrows;

  // assign missing tracks
  for (let i in activeArrows) {
    if (activeArrows[i].track === undefined) {
      const track = freeTracks.shift();

      if (track !== undefined) {
        activeArrows[i].track = track;
      } else {
        //console.warn("no track for ", i,freeTracks,activeArrows);
        activeArrows[i].track = undefined;
      }
    }
  }

  children.push(
    <div style={style} key="arrow-overlay" className="Arrows">
      <Arrows
        height={style.height}
        width={style.width}
        arrows={activeArrows}
        trackSpacing={8}
        firstTrack={16}
        halfLineHeight={halfLineHeight}
        fullLineHeight={fullLineHeight}
      />
    </div>
  );

  return children;
}

function renderAdditionalCell({
  cellCache,
  columnSizeAndPositionManager,
  columnStartIndex,
  columnStopIndex,
  deferredMeasurementCache,
  horizontalOffsetAdjustment,
  isScrolling,
  isScrollingOptOut,
  parent, // Grid (or List or Table)
  rowSizeAndPositionManager,
  rowStartIndex,
  rowStopIndex,
  styleCache,
  verticalOffsetAdjustment,
  visibleColumnIndices,
  visibleRowIndices,
}, x, width, y, height, key, cellRenderer) {
  const areOffsetsAdjusted =
    columnSizeAndPositionManager.areOffsetsAdjusted() ||
    rowSizeAndPositionManager.areOffsetsAdjusted();

  const canCacheStyle = !isScrolling && !areOffsetsAdjusted;

  let style;

  // Cache style objects so shallow-compare doesn't re-render unnecessarily.
  if (canCacheStyle && styleCache[key]) {
    style = styleCache[key];
  } else {
    style = {
      height: height,
      left: x + horizontalOffsetAdjustment,
      position: 'absolute',
      top: y + verticalOffsetAdjustment,
      width: width,
    };

    styleCache[key] = style;
  }

  let cellRendererParams = {
    columnIndex: -1,
    isScrolling,
    isVisible: true,
    key,
    parent,
    rowIndex: -1,
    style,
  };

  let renderedCell;

  // Avoid re-creating cells while scrolling.
  // This can lead to the same cell being created many times and can cause performance issues for "heavy" cells.
  // If a scroll is in progress- cache and reuse cells.
  // This cache will be thrown away once scrolling completes.
  // However if we are scaling scroll positions and sizes, we should also avoid caching.
  // This is because the offset changes slightly as scroll position changes and caching leads to stale values.
  // For more info refer to issue #395
  //
  // If isScrollingOptOut is specified, we always cache cells.
  // For more info refer to issue #1028
  if (
    (isScrollingOptOut || isScrolling) &&
    !horizontalOffsetAdjustment &&
    !verticalOffsetAdjustment
  ) {
    if (!cellCache[key]) {
      cellCache[key] = cellRenderer(cellRendererParams);
    }

    renderedCell = cellCache[key];

    // If the user is no longer scrolling, don't cache cells.
    // This makes dynamic cell content difficult for users and would also lead to a heavier memory footprint.
  } else {
    renderedCell = cellRenderer(cellRendererParams);
  }

  if (renderedCell == null || renderedCell === false) {
    return null;
  }

  if (process.env.NODE_ENV !== 'production') {
    warnAboutMissingStyle(parent, renderedCell);
  }

  return renderedCell;
}

/**
 * Default implementation of cellRangeRenderer used by Grid.
 * This renderer supports cell-caching while the user is scrolling.
 */

function cellRangeRenderer(props, activeArrows, halfLineHeight, fullLineHeight) {
  let {
    cellCache,
    cellRenderer,
    columnSizeAndPositionManager,
    columnStartIndex,
    columnStopIndex,
    deferredMeasurementCache,
    horizontalOffsetAdjustment,
    isScrolling,
    isScrollingOptOut,
    parent, // Grid (or List or Table)
    rowSizeAndPositionManager,
    rowStartIndex,
    rowStopIndex,
    styleCache,
    verticalOffsetAdjustment,
    visibleColumnIndices,
    visibleRowIndices,
  } = props;
  const renderedCells = [];

  // Browsers have native size limits for elements (eg Chrome 33M pixels, IE 1.5M pixes).
  // User cannot scroll beyond these size limitations.
  // In order to work around this, ScalingCellSizeAndPositionManager compresses offsets.
  // We should never cache styles for compressed offsets though as this can lead to bugs.
  // See issue #576 for more.
  const areOffsetsAdjusted =
    columnSizeAndPositionManager.areOffsetsAdjusted() ||
    rowSizeAndPositionManager.areOffsetsAdjusted();

  const canCacheStyle = !isScrolling && !areOffsetsAdjusted;

  for (let rowIndex = rowStartIndex; rowIndex <= rowStopIndex; rowIndex++) {
    let rowDatum = rowSizeAndPositionManager.getSizeAndPositionOfCell(rowIndex);
    let renderedRowCells = [];

    for (
      let columnIndex = columnStartIndex;
      columnIndex <= columnStopIndex;
      columnIndex++
    ) {
      let columnDatum = columnSizeAndPositionManager.getSizeAndPositionOfCell(
        columnIndex,
      );
      let isVisible =
        columnIndex >= visibleColumnIndices.start &&
        columnIndex <= visibleColumnIndices.stop &&
        rowIndex >= visibleRowIndices.start &&
        rowIndex <= visibleRowIndices.stop;
      let key = `${rowIndex}-${columnIndex}`;
      let style;

      // Cache style objects so shallow-compare doesn't re-render unnecessarily.
      if (canCacheStyle && styleCache[key]) {
        style = styleCache[key];
      } else {
        // In deferred mode, cells will be initially rendered before we know their size.
        // Don't interfere with CellMeasurer's measurements by setting an invalid size.
        if (
          deferredMeasurementCache &&
          !deferredMeasurementCache.has(rowIndex, columnIndex)
        ) {
          // Position not-yet-measured cells at top/left 0,0,
          // And give them width/height of 'auto' so they can grow larger than the parent Grid if necessary.
          // Positioning them further to the right/bottom influences their measured size.
          style = {
            height: 'auto',
            left: 0,
            position: 'absolute',
            top: 0,
            width: 'auto',
          };
        } else {
          style = {
            height: rowDatum.size,
            left: columnDatum.offset + horizontalOffsetAdjustment,
            position: 'absolute',
            top: 0,//rowDatum.offset + verticalOffsetAdjustment,
            width: columnDatum.size,
          };

          styleCache[key] = style;
        }
      }

      let cellRendererParams = {
        columnIndex,
        isScrolling,
        isVisible,
        key,
        parent,
        rowIndex,
        style,
      };

      let renderedCell;

      // Avoid re-creating cells while scrolling.
      // This can lead to the same cell being created many times and can cause performance issues for "heavy" cells.
      // If a scroll is in progress- cache and reuse cells.
      // This cache will be thrown away once scrolling completes.
      // However if we are scaling scroll positions and sizes, we should also avoid caching.
      // This is because the offset changes slightly as scroll position changes and caching leads to stale values.
      // For more info refer to issue #395
      //
      // If isScrollingOptOut is specified, we always cache cells.
      // For more info refer to issue #1028
      if (
        (isScrollingOptOut || isScrolling) &&
        !horizontalOffsetAdjustment &&
        !verticalOffsetAdjustment
      ) {
        if (!cellCache[key]) {
          cellCache[key] = cellRenderer(cellRendererParams);
        }

        renderedCell = cellCache[key];

        // If the user is no longer scrolling, don't cache cells.
        // This makes dynamic cell content difficult for users and would also lead to a heavier memory footprint.
      } else {
        renderedCell = cellRenderer(cellRendererParams);
      }

      if (renderedCell == null || renderedCell === false) {
        continue;
      }

      if (process.env.NODE_ENV !== 'production') {
        warnAboutMissingStyle(parent, renderedCell);
      }

      renderedRowCells.push(renderedCell);
    }

    // Render line overlay
    const firstColDatum = columnSizeAndPositionManager
      .getSizeAndPositionOfCell(columnStartIndex);
    const lastColDatum = columnSizeAndPositionManager
      .getSizeAndPositionOfCell(columnStopIndex);
    // HACK: forward address columns props to the wrapping line div
    const addrColProps = renderedRowCells.length > 0 ? getCellProps(renderedRowCells[0]) : {};
    const rowWidth = lastColDatum.offset + lastColDatum.size - firstColDatum.offset;

    if (!Number.isNaN(rowWidth)) {
      const lineOverlay = renderAdditionalCell(
        props, firstColDatum.offset, rowWidth, rowDatum.offset, rowDatum.size,
        `line-overlay-${rowIndex}`,
        ({style, key }) => (
          <div
            from={addrColProps.from}
            to={addrColProps.to}
            style={style} key={key}
            className="Line"
          >
            <ReactHoverObserver>
              {({ isHovering }) => (
                <div style={{width: style.width, height: style.height}} className={!isHovering ? "" : "active"}>
                  {renderedRowCells}
                </div>
              )}
            </ReactHoverObserver>
          </div>));

      if (lineOverlay) {
        renderedCells.push(lineOverlay);
      }
    }
  }

  if (renderedCells.length >= 2) {
    return arrowRenderer(renderedCells, props, activeArrows, halfLineHeight, fullLineHeight);
  } else {
    return renderedCells;
  }
}

function warnAboutMissingStyle(parent, renderedCell) {
  if (process.env.NODE_ENV !== 'production') {
    if (renderedCell) {
      // If the direct child is a CellMeasurer, then we should check its child
      // See issue #611
      if (renderedCell.type && renderedCell.type.__internalCellMeasurerFlag) {
        renderedCell = renderedCell.props.children;
      }

      if (
        renderedCell &&
        renderedCell.props &&
        renderedCell.props.style === undefined &&
        parent.__warnedAboutMissingStyle !== true
      ) {
        parent.__warnedAboutMissingStyle = true;

        console.warn(
          'Rendered cell should include style property for positioning.',
        );
      }
    }
  }
}

export { cellRangeRenderer };
