import React from 'react';
import { Grid, AutoSizer } from 'react-virtualized';
import CreateWidthMeasurer from 'measure-text-width';

import { cellRangeRenderer } from './renderer.js';
import { cellRenderer } from './cells.js';

import './style.css';

class Listing extends React.Component {
  constructor(props) {
    super(props);

    // arrow-id -> arrow
    this.activeArrows = {};
    this.gridRef = React.createRef();
    this.gridScrollTop = 0;
    this.state = {
      mnemonicColumnChars: 0,
      mnemonicColumnWidth: 0,
    };

    const ruler = CreateWidthMeasurer(window).setFont('10px "Source Code Pro"');;
    this.mnemonicCharWidth = ruler("M");
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    // Are we adding new items to the list?
    // Capture the scroll position so we can adjust scroll later.
    return this.gridScrollTop;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // If we have a snapshot value, we've just added new items.
    // Adjust scroll so these new items don't push the old ones out of view.
    // (snapshot here is the value returned from getSnapshotBeforeUpdate)
    if (snapshot !== 0) {
      const grid = this.gridRef.current;
      grid.scrollToPosition({ scrollLeft: 0, scrollTop: snapshot });
    }
  }

  render() {
    const {
      func, listing, scrollToRow, className, addressColumnWidth,
      fullLineHeight, halfLineHeight, backend, mnemonicPaddingLeft,
      mnemonicPaddingRight
    } = this.props;

    return (
      <div className={[className, "Listing"].join(" ")} style={{display: "flex", alignItems: "stretch"}}>
        <div className={"Listing-block-list"} style={{flex: 1}}>
          <style>{`
            :root {
              --full-line-height: ${fullLineHeight}px;
              --half-line-height: ${halfLineHeight}px;
              --address-column-width: ${addressColumnWidth}px;
              --mnemonic-padding-left: ${mnemonicPaddingLeft}px;
              --mnemonic-padding-right: ${mnemonicPaddingRight}px;
              }
            `}
          </style>
          <AutoSizer>
            {({ height, width }) => {
              const columnWidth = ({ index }) => {
                switch (index) {
                  case 0:
                    return addressColumnWidth;
                  case 1:
                    return this.state.mnemonicColumnWidth;
                  case 2:
                    return width - this.state.mnemonicColumnWidth - addressColumnWidth;
                  default:
                    console.error("Unknown column");
                    return 0;
                }
              };
              const myCellRenderer = (props) => {
                const { columnIndex, rowIndex } = props;

                if (columnIndex === 1) {
                  const line = listing.value.lines[rowIndex];
                  const chars =  line.opcode + " " + line.args.map((arg, idx) => arg).join(", ");

                  if (this.state.mnemonicColumnChars < chars.length) {
                    this.setState((prevState) => {
                      prevState.mnemonicColumnChars =
                        Math.max(prevState.mnemonicColumnChars, chars.length);
                      prevState.mnemonicColumnWidth =
                        prevState.mnemonicColumnChars * this.mnemonicCharWidth
                        + mnemonicPaddingLeft + mnemonicPaddingRight;
                      return prevState;
                    });
                  }
                }

                return cellRenderer(props, listing.value.lines, func.uuid, backend);
              };
              const myCellRangeRenderer = (props) => {
                return cellRangeRenderer(props, this.activeArrows,
                  halfLineHeight, fullLineHeight);
              };
              let row = 0;

              if (scrollToRow >= 0 && scrollToRow < listing.value.lines.length) {
                row = scrollToRow;
              }

              return (
                <Grid
                  style={{overflowX: "hidden"}}
                  onScroll={({ scrollTop }) => { this.gridScrollTop = scrollTop; }}
                  ref={this.gridRef}
                  key={"listing-version-" + listing.version + "-col-width-" + this.state.mnemonicColumnChars}
                  scrollToAlignment="start"
                  scrollToRow={row}
                  columnWidth={columnWidth}
                  width={width}
                  cellRenderer={myCellRenderer}
                  columnCount={3}
                  height={height}
                  rowCount={listing.value.lines.length}
                  rowHeight={({ index }) => {
                    if (listing.value.lines[index].isHead) {
                      return fullLineHeight;
                    } else {
                      return halfLineHeight;
                    }
                  }}
                  cellRangeRenderer={myCellRangeRenderer}
                />);
            }}
          </AutoSizer>
        </div>
      </div>
    );
  }
}
export default Listing;
